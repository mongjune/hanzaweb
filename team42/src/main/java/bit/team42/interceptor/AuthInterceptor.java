package bit.team42.interceptor;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import bit.team42.domain.MemberVO;
import bit.team42.service.MemberService;

public class AuthInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);
	@Inject
	private MemberService service;
	
	private void saveDest(HttpServletRequest req) {

		String uri = req.getRequestURI();
		String query = req.getQueryString();

		if (query == null || query.equals("null")) {

			query = "";

		} else {
			query = "?" + query;
		}

		if (req.getMethod().equals("GET")) {
			logger.info("dset" + (uri + query));
			req.getSession().setAttribute("dest", uri + query);
			
		}

	}
	
	
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		HttpSession session  = request.getSession();
		System.out.println("session::::::::::::::::::::::::::::::::"+session);
		
		if(session.getAttribute("login")== null){
			
			logger.info("not login");
			saveDest(request);
			Cookie loginCookie = WebUtils.getCookie(request, "loginCookie");
			
			
			if(loginCookie != null){
				MemberVO memberVO = service.checkLogin(loginCookie.getValue());
				logger.info("MEMBERVO:::::::" + memberVO);
				if(memberVO != null){
			
						session.setAttribute("login", memberVO);
						return true;
					
				}
			}
					
			response.sendRedirect("/login");
			return false;
		}
		
		
		
		return true;
	}
	

}
