package bit.team42.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import bit.team42.domain.Criteria;
import bit.team42.domain.QReplyVO;
import bit.team42.persistence.QReplyDAO;

@Service
public class QReplyService {
	
	@Inject
	private QReplyDAO dao;
	
	public void addReply(QReplyVO vo)throws Exception{
		dao.create(vo);
	}
	
	public List<QReplyVO> listReply(Integer qno) throws Exception{
		return dao.list(qno);
	}
	
	public void modifyReply(QReplyVO vo) throws Exception{
		dao.update(vo);
	}
	
	public void removeReply(Integer qrno) throws Exception{
		dao.delete(qrno);
	}
	
	public List<QReplyVO> listReplyPage(Integer qno, Criteria cri) throws Exception{
		return dao.listPage(qno, cri);
	}
	
	public int count(Integer qno) throws Exception{
		return dao.count(qno);
	}
	
	
	
	

}
