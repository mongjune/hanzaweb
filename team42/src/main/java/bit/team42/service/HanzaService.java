package bit.team42.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import bit.team42.domain.Criteria;
import bit.team42.domain.HanzaSearchType;
import bit.team42.domain.HanzaVO;
import bit.team42.domain.PageMaker;
import bit.team42.persistence.HanzaDAO;
@Service
public class HanzaService {

	@Inject
	HanzaDAO dao;
	
	public HanzaVO selbyhanza(String hanza) {
		return dao.selbyhanza(hanza);
	}

	public List<HanzaVO> selbygrade(HanzaVO hanza, Criteria cri) {
		return dao.selbygrade(hanza,cri);
	}
	public List<HanzaVO> selbywritecount(HanzaVO hanza, Criteria cri) {
		return dao.selbywritecount(hanza,cri);
	}
	public PageMaker gradetotal(HanzaVO hanza,Criteria cri) {
		PageMaker pagemaker = new PageMaker();
		pagemaker.setCri(cri);
		pagemaker.setTotalCount(dao.gradetotal(hanza));
		return pagemaker;
		
	}
	
	public PageMaker writecounttotal(HanzaVO hanza,Criteria cri) {
		PageMaker pagemaker = new PageMaker();
		pagemaker.setCri(cri);
		pagemaker.setTotalCount(dao.writecounttotal(hanza));
		return pagemaker;
		
	}
	public HanzaVO selbymarker(HanzaVO hanza) {
		return dao.selbymarker(hanza);
	}

	
	
	public int insert(HanzaVO hanza) {
		return dao.insert(hanza);
	}
	public int delete(String hanza) {
		return dao.delete(hanza);
	}
	public int update(HanzaVO hanza) {
		System.out.println("������Ʈ post "+hanza);
		return dao.update(hanza);
	}
	
	
	public int gradecnt(HanzaVO hanza) {
		double totalcnt = dao.gradecnt(hanza);
		double temp = totalcnt/10;
		int last = (int) Math.ceil((temp));
		return last;
	}
	
	public PageMaker searchcnt(HanzaSearchType type,Criteria cri) {
		PageMaker pagemaker = new PageMaker();
		pagemaker.setCri(cri);
		pagemaker.setTotalCount(dao.searchlistcnt(type));
		return pagemaker;
	}
	
	public List<HanzaVO> searchlist(HanzaSearchType type) {
		return dao.searchlist(type);
	}
	
}
