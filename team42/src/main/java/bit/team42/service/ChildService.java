package bit.team42.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bit.team42.domain.ChildVO;
import bit.team42.domain.Criteria;

import bit.team42.domain.SearchCriteria;
import bit.team42.persistence.ChildDAO;
import bit.team42.persistence.MemberDAO;

@Transactional
@Service
public class ChildService {

	@Inject
	private ChildDAO dao;
	
	@Inject
	private MemberDAO mdao;
	
	public List<ChildVO> listCriteria(Criteria cri)throws Exception{
		return dao.listCriteria(cri);
	}
	
	public int listCount(Criteria cri)throws Exception{
		return dao.listCount(cri);
	}
	
	public void regist(ChildVO vo, String userid)throws Exception{
		
		if(vo.getImgpath() != null){
			System.out.println("�̹����� �ִ� ���");
			dao.fadd(vo);
		}else{
		    dao.add(vo);
		    System.out.println("�̹����� ������ ���");
		}
		
		mdao.childcntupdate(userid);
		
		System.out.println("���� : "+vo.toString());
		
	}
	
	
	
	public ChildVO read(Integer cno)throws Exception{
		return dao.read(cno);
	}
	
	public void modify(ChildVO vo)throws Exception{
		if(vo.getImgpath() != null){
	         
	         dao.fupdate(vo);
	         
	      }else{
	         dao.update(vo);         
	         
	      }
	}
	public int getmax() throws Exception{
		return dao.maxcno();
	}
	
	public void remove(Integer cno, String userid)throws Exception{
	System.out.println("����");
		dao.remove(cno);
		mdao.childcntupdate(userid);
	}
	

	
	public List<ChildVO>listAll()throws Exception{
		return dao.listAll();
	}
	
	public List<ChildVO>useridlist(String userid)throws Exception{
		return dao.useridlist(userid);
	}
	
	public int listSearchCount(SearchCriteria cri)throws Exception{
		return dao.listSearchCount(cri);
	}
	
	public List<ChildVO> listSearchCri(SearchCriteria cri)throws Exception{
		return dao.listSearch(cri);
	}
}
