package bit.team42.controller;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import bit.team42.domain.MemberVO;
import bit.team42.domain.PageMaker;
import bit.team42.domain.SearchCriteria;
import bit.team42.dto.LoginDTO;
import bit.team42.service.ChildService;
import bit.team42.service.MemberService;


@Controller
@RequestMapping("/user/*")
public class MemberController {

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
	@Inject
	private MemberService service;
	@Inject
	private ChildService cService;
	
	
	@RequestMapping(value="/homePage", method=RequestMethod.GET)
	public void home()throws Exception{
		logger.info("homePage Get.....................");
		
	}
	@RequestMapping(value="/register", method=RequestMethod.GET)
	public void registerGet()throws Exception{
		logger.info("register Get.....................");
		
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public  ResponseEntity<String> registerPost(MemberVO vo, Model model,RedirectAttributes rttr){
		logger.info("register POST.....................");
		try {
			service.register(vo);
		} catch (Exception e) {
			e.printStackTrace();
			rttr.addFlashAttribute("register", "fail");
			return new ResponseEntity<String>("success", HttpStatus.FAILED_DEPENDENCY);
		}
		
		rttr.addFlashAttribute("register", "success");
		
		return new ResponseEntity<String>("success", HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public void read(Model model,@ModelAttribute("cri")SearchCriteria cri,HttpServletRequest req) throws Exception{
		logger.info("read Marker.....");
		HttpSession temp = req.getSession();
		
		MemberVO user = (MemberVO)temp.getAttribute("login");
		
		String userid = user.getUserid();
	
		model.addAttribute("MemberVO",service.readOne(userid));
	}
	


	@RequestMapping(value = "/uUpdate", method = RequestMethod.GET)
	public void updateGet(Model model, @ModelAttribute("cri")SearchCriteria cri, HttpServletRequest req)throws Exception{
		
		HttpSession temp = req.getSession();
		
		MemberVO user = (MemberVO)temp.getAttribute("login");
		
		String userid = user.getUserid();
		
		logger.info(userid);
		
		logger.info("update Get...............");
		
		
		model.addAttribute("MemberVO",service.readOne(userid));
		
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public ResponseEntity<String>  updatePost(MemberVO vo, Model model,RedirectAttributes rttr,SearchCriteria cri){

		System.out.println("UPDATE::::::::::::::POST:::::::::::::::::::");
		
		try {
			service.update(vo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
			return new ResponseEntity<String>("success", HttpStatus.OK);
		}
		rttr.addAttribute("page", cri.getPage());
		rttr.addAttribute("pageNum", cri.getPageNum());
		rttr.addAttribute("searchType", cri.getSearchType());
		rttr.addAttribute("keyword", cri.getKeyword());
		rttr.addFlashAttribute("result", "success");
		
		return new ResponseEntity<String>("success", HttpStatus.OK);
		
	}
	
	
	

	@RequestMapping(value = "/remove", method = RequestMethod.GET)
	public void removeGet(String userid, Model model)throws Exception{
		
		logger.info("remove Get...............");
		userid = "mskang102";
		model.addAttribute("MemberVO",service.readOne(userid));
		
	}
	
	@RequestMapping(value="/remove", method=RequestMethod.POST)
	public String removePost(String userid, Model model,RedirectAttributes rttr){
		logger.info("remove Post.....................");

	
			try {
				service.delete(userid);
			} catch (Exception e) {
				e.printStackTrace();
				rttr.addFlashAttribute("remove", "fail");
				return "redirect:/user/update";
			}
			
			rttr.addFlashAttribute("remove", "�����Ϸ�");
		
			return "redirect:/user/homePage";
	}

	@RequestMapping(value = "/ulist", method = RequestMethod.GET)
	public void list(@ModelAttribute("cri")SearchCriteria cri, Model model)throws Exception{
		
		logger.info("listPage GET..........................");
		logger.info(cri.toString());
		
	
		
		model.addAttribute("list",service.listSearchCri(cri));
		
		PageMaker pageMaker = new PageMaker();
		
		pageMaker.setCri(cri);
		
	
		pageMaker.setTotalCount(service.listSearchCount(cri));
		
		model.addAttribute("pageMaker", pageMaker);
		
	
	}
	
	
	@RequestMapping(value = "/upda", method = RequestMethod.POST)
	public void choice(Model model, @ModelAttribute("cri")SearchCriteria cri, HttpServletRequest req)throws Exception{
		
		HttpSession temp = req.getSession();
		
		MemberVO user = (MemberVO)temp.getAttribute("login");
		
		String userid = user.getUserid();
		
		logger.info(userid);
		
		logger.info("choice Get...............");
		
		
		model.addAttribute("MemberVO",service.readOne(userid));
		
	
	}
	
	@RequestMapping(value = "/childlist", method = RequestMethod.GET)
	public void choice(Model model,HttpServletRequest req)throws Exception{
		
		HttpSession temp = req.getSession();
		MemberVO user = (MemberVO)temp.getAttribute("login");
		String userid = user.getUserid();
		System.out.println(cService.useridlist(userid));
		System.out.println(userid);
		model.addAttribute("MemberVO",service.readOne(userid));
		model.addAttribute("list",cService.useridlist(userid));
		
		
	
	}
	
	
	
	
	


}
