package bit.team42.controller;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import bit.team42.domain.ChildVO;
import bit.team42.domain.Criteria;
import bit.team42.domain.HanzaVO;
import bit.team42.domain.PageMaker;
import bit.team42.service.HanzaService;

@Controller
@RequestMapping("/play")
public class PlayController {
	
	@Inject
	HanzaService hservice;
	
	@RequestMapping(value = "/card/{grade}", method = RequestMethod.GET)
	public String webtest(Model model,@PathVariable("grade")int grade,HttpServletRequest res){
		ChildVO vo = (ChildVO)res.getAttribute("clogin");
		model.addAttribute("childvo", vo);
		
		model.addAttribute("grade", grade);
		if(grade==7){
			return "play/card7";
		}
		return "play/card";
		
	}


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String Home(Model model,HttpServletRequest res){
		ChildVO vo = (ChildVO)res.getSession().getAttribute("clogin");
		model.addAttribute("childvo", vo);
		model.addAttribute("grade", 8);
		return "play/home";
		
	}
	
	 @RequestMapping(value = "voice/study/{grade},{page}", method = RequestMethod.GET)
	   public String study(Model model,HanzaVO vo,Criteria cri){
	      System.out.println(cri);
	      PageMaker pagemaker=hservice.gradetotal(vo, cri);
	      System.out.println(pagemaker + " ..............");
	      List<HanzaVO> list = new ArrayList<HanzaVO>();
	      list = hservice.selbygrade(vo,pagemaker.getCri());
	      model.addAttribute("list", list);
	      model.addAttribute("pagemaker",pagemaker);
	      model.addAttribute("past",list.get(0).getGrade());
	      model.addAttribute("page", cri.getPage());
	     

	      return "play/voice/study";
	   }
	   
	   
	   
	   @RequestMapping(value = "voice/gradedepth/{grade},{page}", method = RequestMethod.GET)
	   public String gradedepth(Model model,HanzaVO vo,Criteria cri){
	      System.out.println(cri);
	      PageMaker pagemaker=hservice.gradetotal(vo, cri);
	      System.out.println(pagemaker + " ..............");
	      List<HanzaVO> list = new ArrayList<HanzaVO>();
	      list = hservice.selbygrade(vo,pagemaker.getCri());
	      model.addAttribute("pagemaker",pagemaker);
	      model.addAttribute("past",list.get(0).getGrade());
	      return "play/voice/gradedepth";
	   }
	   
	   
	   @RequestMapping(value = "voice/block/{grade},{page}", method = RequestMethod.GET)
	   public String block(Model model,HanzaVO vo,Criteria cri){
		   	  List<HanzaVO> list = new ArrayList<HanzaVO>();
		      list = hservice.selbygrade(vo,cri);
		      model.addAttribute("volist", list);
		      model.addAttribute("grade", vo.getGrade());
		      model.addAttribute("page", cri.getPage());
		      return "play/voice/block";
	   }
	   


}