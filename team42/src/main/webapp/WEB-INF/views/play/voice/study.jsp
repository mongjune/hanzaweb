<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@include file="/resources/include/header.jsp"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


<h1> ${vo }</h1>

<div hidden>

<c:forEach begin="${pagemaker.startPage}" end="${pagemaker.endPage }" var="idx" > 
            <li
               <c:out value="${pagemaker.cri.page == idx?'class =active':''}"/>>
               <a href=""  class="pagebtn" custemid = "${past},${idx}">${idx} 단계</a>
            </li>
       </c:forEach>
</div>

   <table>
      
      <c:forEach items="${list}" var="Hanza">
         
         <tr>
         
         <td><a href = "" class="btn btn-info btn-lg readbtn" data-toggle="modal" data-target="#myModal" target=${Hanza.hanza }>  ${Hanza.hanza } </a>  </td>  
         
         <td>  ${Hanza.mean }</td>
      
         </tr>
         
      </c:forEach>
   
    <button  class="btn btn-warning startbtn"  ><span style="margin-right: 5px"></span>벽돌꺠기</button>
   
   </table>



  <div class="modal modal3">
      <div class="modal-content col-md-6 col-md-offset-3"style="margin-top: 5%">
            <div class="modal-header">                
                <h1 class="text-center">한자 </h1>
            </div>
            <div class="modal-body">
            
                    <div class="form-group" id="imgdiv">
                             한자<input type="text" class="form-control input-lg"  readyonly id="rhanzainput"  >
                    </div>
                       <div class="form-group" id="rhanzameaninput">
                            뜻음<input type="text" class="form-control input-lg" readyonly id="rhanzameaninput" >
                    </div>      
                    <div class="form-group" id="writecntinp" >
                            횟수<input type="text" class="form-control input-lg" readyonly id="writecntinp" >
                    </div>  
                               
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <button class="btn cancel"   data-dismiss="modal" aria-hidden="true">Cancel</button>
                </div>
            </div>
        </div>
        </div>



 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

<script> 

$(".readbtn").on("click",function (event) {
   event.preventDefault();
   var idx = $(event.target).attr("target");

     $.get("/hanza/read/"+idx, function(data){
    	console.log(data)
           $('.modal3').css({ "display":"block" });
          $('#rhanzameaninput').html("<center><h3>의미 : "+data.mean+"</h3></center>")
           $('#writecntinp').html("<center>횟수 : "+data.writecount+"</center>")
            $('#imgdiv').html("<center><img src='/img/hanzaimgbb/"+data.hanza+"'></center>")
      });
       
       
   });
   $(".cancel").on("click", function(){
       console.log("cancel");
       $('.modal').css({ "display":"none" });
       
	});
   $(".startbtn").on("click", function(){
       var url = '${past},${page}'
       alert(url)
       self.location = "/play/voice/block/${past},${page}"
       
	});

</script>

</body>
</html>
    <%@include file="/resources/include/footer.jsp"%>