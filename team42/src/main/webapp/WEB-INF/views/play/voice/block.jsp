<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>jQuery UI Effects - Effect demo</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <style>
        .ans{ width: 152px; height:227px;
            position:absolute;
            background-image: url("/resources/img/brick.jpg");
        }
        
        
        #container {
            border: solid;
            width: 760px; height: 454px;
            position: absolute;
            top: 300px;
            left: 500px;
            background-image: url("/resources/img/VoiceHideback.PNG");
        }
    </style>
</head>
<body>
<center>
<button class="gradebtn">단계선택으로 가기</button>
</center>
<div id="container" >
    <c:forEach var="what" items="${volist}">
            <div class="ans" customid = "${what.hanza} " cuslist =  "${what.mean}"></div>
   </c:forEach>
</div>
<c:if test="${page>1}">
<div class = "prev" style=" position:absolute;" >
<img  src = "/img/hanzaimgwb/前">
</div>
</c:if>
<div class = "next" style=" position:absolute;" >
<img  src = "/img/hanzaimgwb/後">
</div>
<select name="effects" id="effectTypes" hidden>
    <option value="explode">Explode</option>
</select>
<script>

    var elements = document.querySelectorAll(".ans")
    var map = new Map();
    for(var cnt=0; cnt<elements.length; cnt++){
        elements[cnt].style.top = 227*(cnt%2)+"px";
        elements[cnt].style.left = 152*(cnt%5)+"px";
    }
	 var recognition = new webkitSpeechRecognition();// webkitSpeechRecognition는 webSpeechAPI 인터페이스를 가지고 있는 객체이다.
    recognition.lang = "ko-KR"; //lang은 언어를 설정하는 속성이다.
    var target;
    var check = false;
    var click = true;
    var last = "${page}";
   $(document).ready(function(targetid) {
	  if(document.querySelector(".prev")){
	   document.querySelector(".prev").style.left = document.querySelector("#container").offsetLeft+"px"
	  }
	  $.get("/hanza/gradecnt/${grade}", function(data){
		 if(last > data-1){
			 $(".next").hide();
		  }
	  });
	  
	   document.querySelector(".next").style.left = document.querySelector("#container").offsetLeft+640+"px"
	   $( ".gradebtn" ).click(function(event) {
    	   self.location = "/play/voice/gradedepth/8,1";
       }) 
	   
       $( ".prev" ).click(function(event) {
    	   self.location = "/play/voice/block/${grade},${page-1}";
       }) 
        $( ".next" ).click(function(event) {
    	   self.location = "/play/voice/block/${grade},${page+1}";
       })
         function runEffect() {
            var selectedEffect = $( "#effectTypes" ).val();
            var options = {};
            target.effect( selectedEffect, options, 500);
         };//깨지는 효과
         
        $( "#container" ).click(function(event) {
            if(click) {
               	target = $(event.target)
                targetsrc = target.attr("customid");
               	if(targetsrc){
               	recognition.start();
                target.css({"background-image": "url(/img/hanzaimgwb/"+targetsrc+")"});
                click = false;
                }
              }
        });
         
         
         
        recognition.onresult = function(event) {
            var result = event.results[0][0].transcript;
            console.log("hanza : "+target.attr("customid"));
            console.log("result: "+event.results[0][0].transcript);
            console.log("result: "+event.results[0][0].transcript);
            var jungdab = target.attr("cuslist");
            jungdab = jungdab.replace(" ", "");
            console.log("jungdab: "+jungdab);
            if(jungdab == result){
            	check = false;
                runEffect(target);
            }else{
            	 check = true;
                 console.log("틀림");
            }
        }
        recognition.onend = function () {
           if(check) {
               recognition.start();
           }else{
               click = true;
           }
       }
    });
</script>
</body>
</html>