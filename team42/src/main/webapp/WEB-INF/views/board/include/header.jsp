<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Developed By M Abdur Rokib Promy">
    <meta name="author" content="cosmic">
    <meta name="keywords" content="Bootstrap 3, Template, Theme, Responsive, Corporate, Business">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>
      	마법천자문 | Home
    </title>

    <!-- Bootstrap core CSS -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/theme.css" rel="stylesheet">
    <link href="/resources/css/bootstrap-reset.css" rel="stylesheet">
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet">-->

    <!--external css-->
    <link href="/resources/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="/resources/css/flexslider.css"/>
    <link href="/resources/assets/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" href="/resources/css/animate.css">
    <link rel="stylesheet" href="/resources/assets/owlcarousel/owl.carousel.css">
    <link rel="stylesheet" href="/resources/assets/owlcarousel/owl.theme.css">

    <link href="/resources/css/superfish.css" rel="stylesheet" media="screen">
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'> -->


    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="/resources/css/component.css">
    <link href="/resources/css/style.css" rel="stylesheet">
    <link href="/resources/css/style-responsive.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="/resources/css/parallax-slider/parallax-slider.css" />
    <script type="text/javascript" src="/resources/js/parallax-slider/modernizr.custom.28468.js">
    </script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js">
    </script>
    <script src="js/respond.min.js">
    </script>
    <![endif]-->
  </head>

  <body>
    <!--header start-->
    <header class="head-section">
      <div class="navbar navbar-default navbar-static-top container">
          <div class="navbar-header">
              <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.html">마법<span>천자문</span></a>
          </div>
          <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                  <li class="dropdown">
                      <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover=
                      "dropdown" data-toggle="dropdown" href="index.html">놀이 <i class="fa fa-angle-down"></i>
                      </a>
                      <ul class="dropdown-menu">
                          <li>
                              <a href="index.html">카드로 학습</a>
                          </li>
                          <li>
                              <a href="index1.html">음성으로 학습</a>
                          </li>

                      </ul>
                  </li>
                  <li class="dropdown">
                      <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover=
                      "dropdown" data-toggle="dropdown" href="#">자료실 <i class="fa fa-angle-down"></i>
                      </a>
                      <ul class="dropdown-menu">
                          <li>
                              <a href="/hanza/searchlist/1">한자 자료실</a>
                          </li>
                          <li>
                              <a href="/hanza/8,1">마커 자료실</a>
                          </li>
                      </ul>
                  </li>
                  <li>
                      <a href="/board/list">자유게시판</a>
                  </li>
                  <li class="dropdown">
                      <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover=
                      "dropdown" data-toggle="dropdown" href="#">마이 페이지 <i class="fa fa-angle-down"></i>
                      </a>
                      <ul class="dropdown-menu">
                          <li>
                              <a href="portfolio-3-col.html">자녀 관리</a>
                          </li>
                          <li>
                              <a href="/user/uUpdate">개인 정보</a>
                          </li>
                        
                      </ul>
                  </li>
                  <li class="dropdown">
                      <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover=
                      "dropdown" data-toggle="dropdown" href="#">Help <i class="fa fa-angle-down"></i>
                      </a>
                      <ul class="dropdown-menu">
                          <li>
                              <a href="/qna/list">Q&A</a>
                          </li>
                       
                          <li>
                              <a href="/notice/list">공지사항</a>
                          </li>
                           <li>
                              <a href="blog-two-col.html">튜토리얼</a>
                          </li>
                      </ul>
                  </li>
                  <li>
                      <a href="/login">로그인</a>
                  </li>
                   <li>
                      <a href="/user/register">회원가입</a>
                  </li>
                  <li><input class="form-control search" placeholder=" Search" type="text"></li>
                  
                   <li class="dropdown">
                      <a class="dropdown-toggle" data-close-others="false" data-delay="0" data-hover=
                      "dropdown" data-toggle="dropdown" href="#">관리자 <i class="fa fa-angle-down"></i>
                      </a>
                      <ul class="dropdown-menu">
                          <li>
                              <a href="/admin/hanza/8,1">한자관리</a>
                          </li>
                          <li>
                              <a href="/admin/member/list">회원관리</a>
                          </li>
                          <li>
                              <a href="/admin/marker/list">마커관리</a>
                          </li>
                          <li>
                              <a href="blog-detail-video.html">통계</a>
                          </li>
                      </ul>
                  </li>
              </ul>
          </div>
      </div>
    </header>
    <!--header end-->
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
