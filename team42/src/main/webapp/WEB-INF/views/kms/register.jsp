<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/resources/include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<style>
.idcheck{
position: relative;border-bottom: 100px; left: 550px; bottom: 530px; border-color: purple;background: purple;
}

</style>
<body>



<article class="container">
    <div class="page-header">
        <h1>회원가입 <small>관리자권한</small></h1>
    </div>

    <div class="col-md-6 col-md-offset-3">
        <form method = "POST" id = "create">
            <div class="form-group">
                <label for="InputEmail">아이디</label> 
                <input type="text" class="form-control" name = "userid" id ="userid" placeholder="아이디">
            </div>
            
            <div class="form-group">
                <label for="password">비밀번호</label>
                <input type="password" class="form-control" name = "userpw" id ="psw" placeholder="비밀번호">
            </div>
            <div class="form-group">
                <label for="pass-check">비밀번호 확인</label>
                <input type="password" class="form-control" id ="pswCheck" placeholder="비밀번호 확인">
                <p class="help-block">비밀번호 확인을 위해 다시한번 입력 해 주세요</p>
            </div>
            <div class="form-group">
                <label>이름</label>
                <input type="text" class="form-control" name = "username" placeholder="이름을 입력해 주세요">
            </div>
            <div class="form-group">
                <label>휴대폰 번호</label>
                <div class="input-group">
                    <input type="text" class="form-control"  name = "phonenumber" placeholder="- 없이 입력해 주세요">

                </div>
            </div>
            <div class="form-group">
                <label>이메일</label>
                <div class="input-group">
                    <input type="text" class="form-control"  name = "email" placeholder="이메일">
                </div>
            </div>
            <div class="form-group">
                <label> 구분</label>
          <select id = "gubun" name = "gubun">
          <option value="a">a</option>
			<option value="u">u</option>
            </select>
                
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-info">회원가입<i class="fa fa-check spaceLeft"></i></button>
                <button type="submit" class="btn btn-warning">가입취소<i class="fa fa-times spaceLeft"></i></button>
                
            </div>
        </form>
         
         <button type="submit"  id="idchecker" class = "idcheck">아이디체크<i class="fa fa-times spaceLeft"></i></button>
      
    </div>

</article>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script >


	
	 window.onload=function(){
		  document.getElementById('create').onsubmit=function(){
		   var pass =document.getElementById('psw').value;
		   var passCheck =document.getElementById('pswCheck').value;
		   
		   if(pass==passCheck){
		    alert('성공');
		   }else{
		    alert('다시입력');
		     return false; 
		   }
		   
		  }
		 }
	 
	 $("#idchecker").on("click",function(e){
		 var id =document.getElementById('userid').value;
		 $.get("/kms/idchecker?userid="+id,function(data){
			 alert(data)
		 })
	 })
	
	
	
	</script>
	
	
	
</body>
</html>
<%@include file="/resources/include/footer.jsp"%>