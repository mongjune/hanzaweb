<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/resources/include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<article class="container">
	<div class="page-header">
		<h1>
			회원가입 <small>관리자권한</small>
		</h1>
	</div>
	<div class="col-md-6 col-md-offset-3">
		<form method="POST" id="update" action = "/kms/update" role = "form">
			<div class="form-group">
				<label for="InputEmail">아이디</label> <input type="text"
					class="form-control" name="userid" value="${MemberVO.userid}"
					placeholder="아이디" readonly="readonly">
			</div>
			<div class="form-group">
				<label for="password">비밀번호</label> <input type="password"
					class="form-control" name="userpw" value="${MemberVO.userpw}"
					id="psw" placeholder="비밀번호">
			</div>
			<div class="form-group">
				<label for="pass-check">비밀번호 확인</label> <input type="password"
					class="form-control" id="pswCheck" value="${MemberVO.userpw}" 
					placeholder="비밀번호 확인">
				<p class="help-block">비밀번호 확인을 위해 다시한번 입력 해 주세요</p>
			</div>
			<div class="form-group">
				<label>이름</label> <input type="text" class="form-control"
					name="username" value="${MemberVO.username}"
					placeholder="이름을 입력해 주세요">
			</div>
			<div class="form-group">
				<label>휴대폰 번호</label>
				<div class="input-group">
					<input type="tel" class="form-control" name="phonenumber"
						value="${MemberVO.phonenumber}" placeholder="- 없이 입력해 주세요">

				</div>
			</div>
			<div class="form-group">
				<label>이메일</label>
				<div class="input-group">
					<input type="text" class="form-control" name="email" value="${MemberVO.email}"
						placeholder="이메일">
				</div>
			</div>
			<input type="hidden" name="page" value="${cri.page}"> <input
				type="hidden" name="pageNum" value="${cri.pageNum}"> <input
				type="hidden" name='searchType' value="${cri.searchType}"> <input
				type="hidden" name='keyword' value="${cri.keyword}">
			<div class="form-group">
				<label>약관 동의</label>
				<div data-toggle="buttons">
					<label class="btn btn-primary active"> <span
						class="fa fa-check"></span> <input id="agree" type="checkbox"
						autocomplete="off" checked>
					</label> <a href="#">이용약관</a>에 동의합니다.
				</div>
			</div>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-info" id ="modify">
					수정<i class="fa fa-check spaceLeft"></i>
				</button>
				<button type="submit" class="btn btn-warning" id ="cancle">
					취소<i class="fa fa-times spaceLeft"></i>
				</button>
			</div>
		</form>
	</div>

	</article>
	  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	<script>
		window.onload = function() {
			document.getElementById('update').onsubmit = function() {
				var pass = document.getElementById('psw').value;
				var passCheck = document.getElementById('pswCheck').value;

				if (pass == passCheck) {
					alert('성공');
				} else {
					alert('틀렸습니다.');
					window.location.reload();
					return false;
					
				}

			}
		}
		

		$(document).ready(function(){
			
			 var formObj = $("form[role='form']");
			    console.log(formObj);

			$("#modify").on("click", function() {
			   self.location = "/kms/list?page=${cri.page}&pageNum=${cri.pageNum}"+"&searchType=${cri.searchType}&keyword=${cri.keyword}";
			});

			


		})
		
	</script>



</body>
</html>
<%@include file="/resources/include/footer.jsp"%>