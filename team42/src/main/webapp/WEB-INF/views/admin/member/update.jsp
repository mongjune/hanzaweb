<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/resources/include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<article class="container">
	<div class="page-header">
		<h1>
			회원수정 <small>관리자권한</small>
		</h1>
	</div>
	<div class="col-md-6 col-md-offset-3">
		
			<div class="form-group">
				<label for="InputEmail">아이디</label> <input type="text" id = "userid"
					class="form-control" name="userid" value="${MemberVO.userid}"
					placeholder="아이디" readonly="readonly">
			</div>
			<div class="form-group">
				<label for="password">비밀번호</label> <input type="password"
					class="form-control" name="userpw" value="${MemberVO.userpw}"
					id="userpw" placeholder="비밀번호">
			</div>
			<div class="form-group">
				<label for="pass-check">비밀번호 확인</label> <input type="password"
					class="form-control" id="pswCheck" value="${MemberVO.userpw}" 
					placeholder="비밀번호 확인">
				<p class="help-block">비밀번호 확인을 위해 다시한번 입력 해 주세요</p>
			</div>
			<div class="form-group">
				<label>이름</label> <input type="text" class="form-control"
					name="username" value="${MemberVO.username}" id = "username"
					placeholder="이름을 입력해 주세요">
			</div>
			<div class="form-group">
				<label>휴대폰 번호</label>
				<div class="input-group">
					<input type="tel" class="form-control" name="phonenumber" id = "phonenumber"
						value="${MemberVO.phonenumber}" placeholder="- 없이 입력해 주세요">

				</div>
			</div>
			<div class="form-group">
				<label>이메일</label>
				<div class="input-group">
					<input type="text" class="form-control" name="email" value="${MemberVO.email}" id = "email"
						placeholder="이메일">
				</div>
			</div>
		
			<input type="hidden" name="page" value="${cri.page}" id = "page"> 
			<input type="hidden" name="pageNum" value="${cri.pageNum}" id = "pagenum"> 
			<input type="hidden" name='searchType' value="${cri.searchType}" id = "searchtype"> 
				<input type="hidden" name='keyword' value="${cri.keyword}" id = "keyword">
			
			
			<div class="form-group text-center">
				<button type="submit" class="btn btn-info" id ="modify">
					수정<i class="fa fa-check spaceLeft"></i>
				</button>
				<button type="submit" class="btn btn-warning" id ="cancle">
					취소<i class="fa fa-times spaceLeft"></i>
				</button>
			</div>

	</div>

	</article>
	  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	<script>
			var formData = new FormData();
		

		 $("#modify").on("click", function(event){
			   
			   var pass =document.getElementById('userpw').value;
			   var passCheck =document.getElementById('pswCheck').value;
			   if(pass!=passCheck){
				    alert('패스워드 다시입력');
				return false;
				   }
			   
			   
			   formData.append("userid", $("#userid").val());
	           formData.append("userpw", $("#userpw").val());
	           formData.append("username", $("#username").val());
	           formData.append("phonenumber", $("#phonenumber").val());
	           formData.append("email", $("#email").val());
	           formData.append("gubun", $("#gubun").val());
	           formData.append("gubun", $("#page").val());
	           formData.append("gubun", $("#pagenum").val());
	           formData.append("gubun", $("#searchtype").val());
	           formData.append("gubun", $("#keyword").val());
	           console.log(formData);
			   
	           $.ajax({
	               url : '/admin/member/update',
	               data : formData,
	               datatype : 'text',
	               processData : false,
	               contentType : false,
	               type : 'POST',
	               success : function(data) {
	                  alert("등록");
	                  alert($("#page").val())
	                 
	                  
	                  location.href = "/admin/member/list?page=${cri.page}&pageNum=${cri.pageNum}"+"&searchType=${cri.searchType}&keyword=${cri.keyword}";
	               }
	            })
	         })
		 
	
		 $("#cancle").on("click", function(){
	            location.href = "/admin/member/list?page=${cri.page}&pageNum=${cri.pageNum}"+"&searchType=${cri.searchType}&keyword=${cri.keyword}";
	         })
		
		
		
		
		
		
	
		
	</script>



</body>
</html>
<%@include file="/resources/include/footer.jsp"%>