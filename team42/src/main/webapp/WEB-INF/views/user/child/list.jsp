<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page session="false"%>
<%@include file="/resources/include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-1.11.1.js"></script>
<style>
      /* Overall & panel width defined using css in MovingBoxes version 2.2.2+ */
      #slider-one { width: 300px; }
      #slider-one > li { width: 150px; }

      #slider-two { width: 500px; }
      #slider-two > div { width: 360px; }
</style>
</head>
<body>

<div class="gray-bg">
    <div class="fof">
            <!-- 404 error -->
            
        <div class="container  error-inner wow flipInX">
        <button class="mbtn btn btn-info btn-lg col-lg-1 col-lg-offset-11" onClick="self.location='/user/child/register'">+</button>           
           <div class="">
          <div class="container">
          <div class="about-carousel wow fadeInLeft" style="width:100%; height:100%">
          <c:if test="${list[0].s_imgpath!=null }">
            <div id="myCarousel" class="carousel slide">
              <!-- Carousel items -->
              <div class="carousel-inner" >
              
                <div class="active item blog-img">  
                   
                   <c:if test="${list[0].s_imgpath!=null }">
                       <img class="img-circle" src="/admin/child/downimg?fileName=${list[0].s_imgpath }" alt="">
                    </c:if>
                    <c:if test="${list[0].s_imgpath ==null }">
                    <img class="img-circle"  src="/admin/child/downimg?fileName=default.png">
                 </c:if>
                    
                       <div class="carousel-caption">
                       <button type="button" class="mbtn btn btn-info btn-lg" data-idx="${list[0].cno }" data-toggle="modal" data-target="#myModal">수정</button>                
                     <button type="button" class="dbtn btn btn-info btn-lg" data-idx="${list[0].cno }" data-toggle="modal">삭제</button>
                     <button type="button" class="choicBtn btn btn-info btn-lg" data-idx="${list[0].cno }" data-toggle="modal">선택</button>
                          <p>
                            ${list[0].cname }
                          </p>
                      </div>                 
                  
                </div>
                <c:forEach items="${list }" begin="1"  var ="child">
                <div class="item blog-img">
                   <c:if test="${child.s_imgpath!=null }">
                       <img class="pos img-circle" src="/admin/child/downimg?fileName=${child.s_imgpath }">
                 </c:if>
                 <c:if test="${child.s_imgpath ==null }">
                    <img class="pos"  src="/admin/child/downimg?fileName=default.png">
                 </c:if>
                  <%-- <img class="img-circle" src="/admin/child/downimg?fileName=${child.s_imgpath }" alt=""> --%>
                  <div class="carousel-caption">
                   <button type="button" class="mbtn btn btn-info btn-lg" data-idx="${child.cno }" data-toggle="modal" data-target="#myModal">수정</button>                
                     <button type="button" class="dbtn btn btn-info btn-lg" data-idx="${child.cno }" data-toggle="modal">삭제</button>
                     <button type="button" class="choicBtn btn btn-info btn-lg" data-idx="${child.cno }" data-toggle="modal">선택</button>
                    <p>
                      ${child.cname }
                    </p>
                  </div>
                </div>
                </c:forEach>                
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                      <i class="fa fa-angle-left">
                      </i>
                    </a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">
                      <i class="fa fa-angle-right">
                      </i>
                    </a>
                  </div>
                  </c:if>
            <c:if test="${list[0].s_imgpath ==null }">
                    <img class="pos"  src="/admin/child/downimg?fileName=NO.png">
           </c:if>
            
          </div>
          
          
 </div>        
</div>            


        
        <!-- /404 error -->
        </div>
    </div>
<!-- modify modal -->
<div class="container">  
  
  <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h1 class="text-center">자녀수정</h1>
             
           </div>
           <div class="modal-body">
             <div class="form-group col-md-6 col-md-offset-6">
                                 아이이름<input type="text" class="form-control input-lg"  name = "cname"  >
                       </div>
                          <div class="form-group col-md-6 col-md-offset-6">
                                 학교<input type="text" class="form-control input-lg" name = "school" >
                       </div>                    
                       <div class="form-group col-md-6 col-md-offset-6">
                                 나이<input type="text" class="form-control input-lg" name = "age" >
                       </div>
                       
                       <div class="form-group col-md-6 col-md-offset-6">
                                 급수<input type="text" class="form-control input-lg" name = "clevel" >
                       </div>
                       
                       <div class="form-group col-md-6 col-md-offset-6">
                                 단계<input type="text" class="form-control input-lg" name = "deep" >
                       </div>
                       
                       <div id="dropzone" style="position:absolute; left:0px; top:0px; ">
                           <img style="width:239px; height:239px"  id="thumImg" src="">          
                       </div>   
           </div>
           <div class="modal-footer">
           <button class="btn btn-primary btn-lg btn-block" id='updatebtn'>수정</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
         </div>
         
       </div>
     </div>
     
   </div>
   
        <!-- end modal -->
        
        <script type="text/javascript">
        
        $(document).ready(function () {
           var test;
           var formData = new FormData();
           
            $('.mbtn').on('click', function (event) {
               event.originalEvent.preventDefault();               
                
                var cno = $(event.target).attr("data-idx");
                
                console.log("수정: "+cno);
                //$('.modal').css({ "display":"block" }); 
                
                   $.ajax({
                    type : 'get',
                    url : '/admin/child/Uread?cno=' + cno,                   
                    dataType : 'text',
                    success : function(result) {
                       //console.log(result);
                       child = $.parseJSON(result);                       
                       
                       console.log(child.cno);
                                         
                       $("input[name=cname]").val(child.cname);
                       $("input[name=school]").val(child.school);
                       $("input[name=age]").val(child.age);
                       $("input[name=clevel]").val(child.clevel);
                       $("input[name=deep]").val(child.deep);
                       $("#updatebtn").attr("data-idx", cno); 
                       var path = "/user/child/downimg?fileName="+child.s_imgpath
                       $("#thumImg").attr("src", path);  
                       
                    }
                 }); 
               
            });
            
            $("#cancel").on("click", function(){
               console.log("cancel");
               $('.modal').css({ "display":"none" });
            });
            
           $('.dbtn').on('click', function(){
              var cno = $(event.target).attr("data-idx");
              console.log("삭제: "+cno);
              
              $.ajax({
                   type : 'delete',
                   url : '/user/child/remove?cno=' + cno,                   
                   dataType : 'text',
                   success : function(result) {
                      //console.log(result);
                      alert('삭제되었습니다.');
                      self.location="/user/child/list"
                   }
                });
           });
           
           $('.choicBtn').on('click', function(){

               var formData = new FormData();
               
               
               var cno = $(event.target).attr("data-idx");
                var userid = $(event.target).attr("data-userid");
                
                console.log("선택: "+cno+userid);
                
               formData.append("userid",userid);
          formData.append("cno", cno);
       
          
          $.ajax({
             url : '/user/child/choice',
             data : formData,
             datatype : 'text',
             processData : false,
             contentType : false,
             type : 'POST',
             success : function(data) {
                alert(data);
                location.href = "/user/child/list";
             }
          })

          
             });
           
           $('#updatebtn').on('click', function(event){
              var cno = $(event.target).attr("data-idx");
              var cname = $("input[name=cname]").val();
               var school = $("input[name=school]").val();
               var age = $("input[name=age]").val();
               var clevel = $("input[name=clevel]").val();
               var deep = $("input[name=deep]").val();
               
               formData.append("cname", cname);
               formData.append("school", school);
               formData.append("age", age);
               formData.append("clevel", clevel);
               formData.append("deep", deep);
               formData.append("cno", cno);
              
              $.ajax({
                   type : 'post',
                   url : '/admin/child/modifys',                   
                   data : formData,
               datatype : 'text',
               processData : false,
               contentType : false,
                   success : function(result) {
                      //console.log(result);
                      alert('수정되었습니다.');
                      self.location="/user/child/list"
                   }
                });
           });
           
           var $dropzone = $('#dropzone');
           $dropzone.on("dragenter dragover", function(event) {
               event.preventDefault();
           });

           $dropzone.on("drop", function(event) {
               event.preventDefault();
                var file = event.originalEvent.dataTransfer.files[0];
                console.log(file);
               formData.append("file", file);
                 console.log(formData);
               //$("#imgname").val(file.name);
               
                  var reader = new FileReader();
                   reader.onload =function (e) {
                       /* $("#dropzone").css({"background-image" : "url("+e.target.result+")" }); */
                       $('#thumImg').attr('src', e.target.result);
                     /*$('#c_img').attr('class', 'thumbnail'); */
                       //console.log(e.target.result);               
                  
               }
               
               reader.readAsDataURL(file);
               
           });
           
           $('.blog-img').hover(
              function(){              
                 $(this).find('.image-caption').slideDown(500); //.fadeIn(250)
              }
              ,
               function(){
                 $(this).find('.image-caption').slideUp(500); //.fadeOut(205)                            
                 });
           
            
        });
        

        </script>


</body>
</html>
<%@include file="/resources/include/footer.jsp"%>