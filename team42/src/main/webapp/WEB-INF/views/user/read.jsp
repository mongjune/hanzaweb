<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/resources/include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>


<article class="container">
	<div class="page-header">
		<h1>
			회원수정 <small>user</small>
		</h1>
	</div>
<form role="form" method="post">
	<div class="col-md-6 col-md-offset-3">

			<div class="form-group">
				<label for="InputEmail">아이디</label> <input type="text"
					class="form-control" name="userid" value="${MemberVO.userid}"
					placeholder="아이디" readonly="readonly">
			</div>
			<div class="form-group">
				<label for="password">비밀번호</label> <input type="password"
					class="form-control" name="userpw" value="${MemberVO.userpw}"
					id="psw" placeholder="비밀번호" readonly="readonly">
			</div>
			<div class="form-group">
				<label for="pass-check">비밀번호 확인</label> <input type="password" value="${MemberVO.userpw}"
					class="form-control" id="pswCheck" value="${MemberVO.userpw}" 
					placeholder="비밀번호 확인" readonly="readonly">
				<p class="help-block">비밀번호 확인을 위해 다시한번 입력 해 주세요</p>
			</div>
			<div class="form-group">
				<label>이름</label> <input type="text" class="form-control"
					name="username" value="${MemberVO.username}"
					placeholder="이름을 입력해 주세요" readonly="readonly">
			</div>
			<div class="form-group">
				<label>휴대폰 번호</label>
				<div class="input-group">
					<input type="tel" class="form-control" name="phonenumber"
						value="${MemberVO.phonenumber}" placeholder="- 없이 입력해 주세요" readonly="readonly">

				</div>
			</div>
			<div class="form-group">
				<label>이메일</label>
				<div class="input-group">
					<input type="text" class="form-control" name="email" value="${MemberVO.email}"
						placeholder="이메일" readonly="readonly">
				</div>
			</div>
			
			<button type="submit" class="btn btn-warning col-lg-3" id="btnModify"><span style="margin-right: 5px"></span>MODIFY</button>
            <button type="submit"   class="btn btn-primary col-lg-3" id="team42"><span style="margin-right: 5px"></span>cancle</button>
		
	</div>

		
 			


	
			
</form>
	</article>





<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script>

$(document).ready(function(){

	var formObj = $("form[role='form']");

	console.log(formObj);

	$("#btnModify").on("click", function() {
		formObj.attr("action", "/user/uUpdate");
		formObj.attr("method", "get");
		formObj.submit();
	});



	$("#team42").on("click", function() {
		formObj.attr("method", "get");
		formObj.attr("action", "/team42");
		formObj.submit();
	});

})

</script>




  
</body>
</html>
<%@include file="/resources/include/footer.jsp"%>