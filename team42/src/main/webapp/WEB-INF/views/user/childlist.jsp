<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page session="false"%>
<%@include file="/resources/include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="http://code.jquery.com/jquery-1.11.1.js"></script>
</head>
<body>

<div class= "childList col-md-offset-1 col-md-10">

<c:forEach  begin="0" end="2"  var ="idx">
   <div class=" wow fadeInRight animated  col-md-4 " data-wow-animation-name="fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
      <div class="blog-img">
         <div class="image-caption">
               <button type="button" class="btn btn-info btn-lg" data-idx="${list[idx].cno }" data-toggle="modal" data-target="#myModal">Open Modal</button>                
             </div>
          <c:if test="${list[idx].s_imgpath!=null }">
              <img id='imgs' class="img-circle" src="/admin/child/downimg?fileName=${list[idx].s_imgpath }">
           </c:if>
           <c:if test="${list[idx].s_imgpath ==null }">
              <img id='imgs' width src="/admin/child/downimg?fileName=_s_0cfb66a2-2fa9-4347-9be5-c0e668c75d80_1.png">
           </c:if>
       </div>
       <div class="blog-content text-center">
          <h3>${list[idx].cname}</h3>
           <p></p>
           
       </div>
       
    </div>
 </c:forEach>
 <nav class="da-arrows">
        <span class="da-arrows-prev">
        </span>
        <span class="da-arrows-next">
        </span>
      </nav>
</div>
            
<div>

</div>
<button onClick="self.location='/admin/child/register';">자녀등록</button>

<!-- modify modal -->
<div class="container">  
  
  <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h1 class="text-center">자녀수정</h1>
             
           </div>
           <div class="modal-body">
             <div class="form-group col-md-6 col-md-offset-6">
                                 아이이름<input type="text" class="form-control input-lg"  name = "cname"  >
                       </div>
                          <div class="form-group col-md-6 col-md-offset-6">
                                 학교<input type="text" class="form-control input-lg" name = "school" >
                       </div>                    
                       <div class="form-group col-md-6 col-md-offset-6">
                                 나이<input type="text" class="form-control input-lg" name = "age" >
                       </div>
                       
                       <div class="form-group col-md-6 col-md-offset-6">
                                 급수<input type="text" class="form-control input-lg" name = "clevel" >
                       </div>
                       
                       <div class="form-group col-md-6 col-md-offset-6">
                                 단계<input type="text" class="form-control input-lg" name = "deep" >
                       </div>
                       
                       <div id="dropzone" class="" style="height: 50%;width:  50%; background-color: crimson">
                        <div id="s_img" style="height: 250px;width:  250px;">                           
                        </div>
                        </div>   
           </div>
           <div class="modal-footer">
           <button class="btn btn-primary btn-lg btn-block" id='updatebtn'>수정</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
         </div>
         
       </div>
     </div>
     
   </div>
   
        <!-- end modal -->
        
        <script type="text/javascript">
        
        $(document).ready(function () {
           var test;
           var formData = new FormData();
           
            $('.btn').on('click', function (event) {
               event.originalEvent.preventDefault();
                console.log($(event.target).attr("data-idx"));
                
                var cno = $(event.target).attr("data-idx");
                $('.modal').css({ "display":"block" }); 
                
                   $.ajax({
                    type : 'get',
                    url : '/admin/child/Uread?cno=' + cno,                   
                    dataType : 'text',
                    success : function(result) {
                       //console.log(result);
                       child = $.parseJSON(result);                       
                       
                       console.log(child.cno);
                                         
                        $("input[name=cname]").val(child.cname);
                       $("input[name=school]").val(child.school);
                       $("input[name=age]").val(child.age);
                       $("input[name=clevel]").val(child.clevel);
                       $("input[name=deep]").val(child.deep);
                       $("#updatebtn").attr("data-idx", cno); 
                       var path = "/admin/child/downimg?fileName="+child.s_imgpath
                       //$("#c_img").attr("src", path);                     
                       $("#dropzone").css({ "background-image" : "url("+path+")" });
                    }
                 }); 
               
            });
            
            $("#cancel").on("click", function(){
               console.log("cancel");
               $('.modal').css({ "display":"none" });
            });
            
           $('.dbtn').on('click', function(){
              var cno = $(event.target).attr("data-idx");
              $.ajax({
                   type : 'delete',
                   url : '/admin/child/remove?cno=' + cno,                   
                   dataType : 'text',
                   success : function(result) {
                      //console.log(result);
                      alert('삭제되었습니다.');
                      self.location="/user/childlist"
                   }
                });
           });
           
           
           $('#updatebtn').on('click', function(event){
              var cno = $(event.target).attr("data-idx");
              var cname = $("input[name=cname]").val();
               var school = $("input[name=school]").val();
               var age = $("input[name=age]").val();
               var clevel = $("input[name=clevel]").val();
               var deep = $("input[name=deep]").val();
               
               formData.append("cname", cname);
               formData.append("school", school);
               formData.append("age", age);
               formData.append("clevel", clevel);
               formData.append("deep", deep);
               formData.append("cno", cno);
              
              $.ajax({
                   type : 'post',
                   url : '/admin/child/modifys',                   
                   data : formData,
               datatype : 'text',
               processData : false,
               contentType : false,
                   success : function(result) {
                      //console.log(result);
                      alert('수정되었습니다.');
                      self.location="/user/childlist"
                   }
                });
           });
           
           var $dropzone = $('#dropzone');
           $dropzone.on("dragenter dragover", function(event) {
               event.preventDefault();
           });

           $dropzone.on("drop", function(event) {
               event.preventDefault();
                var file = event.originalEvent.dataTransfer.files[0];
                console.log(file);
               formData.append("file", file);
                 console.log(formData);
               //$("#imgname").val(file.name);
               
                  var reader = new FileReader();
                   reader.onload =function (e) {
                       $('#c_img').attr('src', e.target.result);
                       $('#c_img').attr('class', 'thumbnail');
                       //console.log(e.target.result);               
                  
               }
               
               reader.readAsDataURL(file);
               
           });
           
           $('.blog-img').hover(
              function(){              
                 $(this).find('.image-caption').slideDown(500); //.fadeIn(250)
              }
              ,
               function(){
                 $(this).find('.image-caption').slideUp(500); //.fadeOut(205)                            
                 });
           
            
        });
        

        </script>


</body>
</html>
<%@include file="/resources/include/footer.jsp"%>