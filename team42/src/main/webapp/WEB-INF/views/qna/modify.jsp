<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/../resources/include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<div class="col-lg-8 col-lg-offset-2">
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">MODIFY QnA</h3>
	</div>
	<!-- /.box-header -->
	<!-- form start -->
	<form role="form" action="modify" method="post">
		<input type='hidden' name='page' value="${cri.page }">
		<input type='hidden' name='pageNum' value="${cri.pageNum }">	
		<input type='hidden' name='searchType' value="${cri.searchType }">	
		<input type='hidden' name='keyword' value="${cri.keyword }">	
	
		<div class="box-body">
			<div class="form-group">
				<label for="exampleInputEmail1">Title</label> <input type="text"
					name='title' class="form-control" placeholder="Enter Title" value="${qnaVO.title}">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Content</label>
				<textarea class="form-control" name="content" rows="3"
					placeholder="Enter ..." >${qnaVO.content}</textarea>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">Writer</label> <input type="text"
					name="userid" class="form-control" placeholder="Enter Writer" readonly="readonly" value="${qnaVO.userid}">
			</div>
		</div>
	</form>
<div class="box-footer">
      <button type="submit" class="btn btn-danger">수정</button>
      <button type="submit" class="btn btn-primary">취소</button>
   </div>
</div>
</div>

   <script
      src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

   <script>
      $(document).ready(function() {

         var formObj = $("form[role='form']");

         console.log(formObj);

         

         $(".btn-danger").on("click", function() {
        	 formObj.submit();
             console.log(formObj);
         });

         $(".btn-primary").on("click", function() {
        	 formObj.submit();
             console.log(formObj);
             self.location = "/qna/list?page=${cri.page}&pageNum=${cri.pageNum}"
            		 +"&searchType = ${cri.searchType}&keyword=${cri.keyword}";
         });

      });
   </script>

</body>
</html>
<%@include file="/../resources/include/footer.jsp"%>
