<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/resources/include/header.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
    <div class="gray-bg">
    <div class="fof">
            <!-- 404 error -->
        <div class="container  error-inner wow flipInX">
            <h1>Team42, Page has been fixed...</h1>
            <p class="text-center">빠른 시일 내에 만들도록 하겠습니다..</p>
            <a class="btn btn-info" href="/login/">GO TO THE LOGIN PAGE</a>
        </div>
        <!-- /404 error -->
        </div>
    </div>
</body>
</html>
<%@include file="/resources/include/footer.jsp"%>